/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getPersonDetails(){
		let fullName = prompt("What is your name?"); 
		let age = prompt("How old are you?"); 
		let location = prompt("Where do you live?");
		alert("Thank you for your input!")

		console.log("Hello,  "+fullName +"\nYou're "+age+" years old.\n You live in "+location);
	}
	getPersonDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//second function here:
	function printArtists(){
		let artists = ["COLN", "Mistah Lefty", "Coldplay", "Midnasty","N.W.A"];

		console.log("1. "+artists[0]+ "\n2. "+artists[1]+ "\n3. "+artists[2]+ "\n4. "+artists[3]+ "\n5. "+artists[4]);
	}
	printArtists();
	

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function printMovieRatings(){
		let movie1 = ["Saving Private Ryan", 94]
		let movie2 = ["All Quite on the Western Front", 92]
		let movie3 = ["Enola Holmes 2", 93]
		let movie4 = ["Demon Slayer", 98]
		let movie5 = ["Sea Beast", 94]

		console.log("1. "+movie1[0]+"\n Rotten Tomatoes Rating: "+ movie1[1]+ "\% \n");
		console.log("2. "+movie2[0]+"\n Rotten Tomatoes Rating: "+ movie2[1] +"\% \n");
		console.log("3. "+movie3[0]+"\n Rotten Tomatoes Rating: "+ movie3[1]+ "\% \n");
		console.log("4. "+movie4[0]+"\n Rotten Tomatoes Rating: "+ movie4[1]+ "\% \n");
		console.log("5. "+movie5[0]+"\n Rotten Tomatoes Rating: "+ movie5[1]+ "\% \n");
	}

	printMovieRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);